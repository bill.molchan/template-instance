package com.qomplx.quickstart.routes

import cats.effect.Sync
import cats.implicits._
import io.circe.{Encoder, Decoder}
import io.circe.generic.semiauto._
import sttp.client3._
import sttp.client3.circe._
import org.http4s.{EntityEncoder, EntityDecoder}
import org.http4s.circe._

trait Jokes[F[_]]{
  def get: F[Jokes.Joke]
}

object Jokes {
  def apply[F[_]](implicit ev: Jokes[F]): Jokes[F] = ev

  final case class Joke(joke: String) extends AnyVal
  object Joke {
    implicit val jokeDecoder: Decoder[Joke] = deriveDecoder[Joke]
    implicit def jokeEntityDecoder[F[_]: Sync]: EntityDecoder[F, Joke] =
      jsonOf
    implicit val jokeEncoder: Encoder[Joke] = deriveEncoder[Joke]
    implicit def jokeEntityEncoder[F[_]]: EntityEncoder[F, Joke] =
      jsonEncoderOf
  }

  final case class JokeError(e: Throwable) extends RuntimeException

  def impl[F[_]: Sync](C: SttpBackend[F, Any]): Jokes[F] = new Jokes[F]{
    def get: F[Jokes.Joke] = for {
      res  <- basicRequest.get(uri"https://icanhazdadjoke.com/").response(asJson[Joke]).send(C)
      joke <- Sync[F].fromEither(res.body).adaptError{ case t => JokeError(t)} // Prevent Client Json Decoding Failure Leaking
    } yield joke
  }
}
