package com.qomplx.quickstart

import scala.concurrent.ExecutionContext
import cats.effect.{Blocker, ConcurrentEffect, ContextShift, Timer}
import cats.implicits._
import fs2.Stream
import sttp.client3.http4s.Http4sBackend
/*
import sttp.tapir._
import sttp.tapir.server.http4s.Http4sServerInterpreter
*/
import org.http4s.implicits._
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.middleware.Logger
import scala.concurrent.ExecutionContext.global

import routes._

object QuickstartServer {
  def stream[F[_]: ConcurrentEffect](implicit cs: ContextShift[F], T: Timer[F], ec: ExecutionContext): Stream[F, Nothing] = {
    for {
      blocker <- Stream.resource(Blocker[F])
      client <- Stream.resource(Http4sBackend.usingDefaultBlazeClientBuilder(blocker, ec))
      helloWorldAlg <- Stream.emit(HelloWorld.impl[F])
      jokeAlg <- Stream.emit(Jokes.impl[F](client))

      // Combine Service Routes into an HttpApp.
      // Can also be done via a Router if you
      // want to extract a segments not checked
      // in the underlying routes.
      httpApp <- Stream.emit((
        QuickstartRoutes.helloWorldRoutes[F](helloWorldAlg) <+>
        QuickstartRoutes.jokeRoutes[F](jokeAlg)
      ).orNotFound)

      // With Middlewares in place
      finalHttpApp <- Stream.emit(Logger.httpApp(true, true)(httpApp))

      exitCode <- BlazeServerBuilder[F](global)
        .bindHttp(8081, "0.0.0.0")
        .withHttpApp(finalHttpApp)
        .serve
    } yield exitCode
  }.drain
}
