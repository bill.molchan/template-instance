package com.qomplx.quickstart

import cats.effect.{Blocker, ExitCode, IO}
import doobie.Transactor
import fs2.aws.s3.S3
import com.monovore.decline._
import com.monovore.decline.effect.CommandIOApp
import cats.implicits._

import scala.concurrent.ExecutionContext.Implicits.global

case class Config[F[_]](
  val producerTopic: String,
  val consumerTopic: String,
  val blocker: Blocker,
  val xa: Transactor[F],
  val s3: S3[F]
)

object Main extends CommandIOApp("quickstart", "The quickstart service. Constructed with the qomplx-service-template.g8 giter8 template.") {
  val producerTopicOpts: Opts[Option[String]] = Opts.option[String]("producer-topic", short="p", help="The Kafka producer topic").orNone
  val consumerTopicOpts: Opts[Option[String]] = Opts.option[String]("consumer-topic", short="c", help="The Kafka consumer topic").orNone
  val jdbcUrlOpts: Opts[Option[String]]       = Opts.option[String]("jdbc-url", short="j", help="The database's JDBC URL").orNone
  val s3BucketOpts: Opts[Option[String]]      = Opts.option[String]("s3-bucket", short="s", help="The S3 bucket name").orNone

  override def main: Opts[IO[ExitCode]] =
    (producerTopicOpts, consumerTopicOpts, jdbcUrlOpts, s3BucketOpts).mapN { (producerTopicO, consumerTopicO, jdbcUrlO, s3BucketO) =>
      Blocker[IO].use { blocker =>
        print(Vector.apply[Any](blocker, producerTopicO, consumerTopicO, jdbcUrlO,  s3BucketO).take(0))//Working around wart remover while playing around
        QuickstartServer.stream[IO].compile.drain.as(ExitCode.Success)
      }
    }
}
