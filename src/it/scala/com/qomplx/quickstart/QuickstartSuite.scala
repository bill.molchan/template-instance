package com.qomplx.quickstart

import munit._

import org.scalacheck.Prop._

import com.dimafeng.testcontainers.lifecycle._
import com.dimafeng.testcontainers.{ ContainerDef, GenericContainer, KafkaContainer, LocalStackV2Container, PostgreSQLContainer }
import com.dimafeng.testcontainers.munit.TestContainersForAll

import org.testcontainers.containers.Network
import org.testcontainers.containers.wait.strategy.Wait
import org.testcontainers.containers.localstack.{LocalStackContainer => LS}

import cats.effect.IO

import fs2.kafka._

class SchemaRegistryContainer(bootstrapServers: String) extends GenericContainer(
  "confluentinc/cp-schema-registry:" + KafkaContainer.defaultTag,
  exposedPorts=List(8081),
  env = Map(
    "SCHEMA_REGISTRY_KAFKASTORE_BOOTSTRAP_SERVERS" -> bootstrapServers,
    "SCHEMA_REGISTRY_HOST_NAME" -> "localhost"
  ),
  waitStrategy=Option(Wait.forHttp("/"))) {

}

object SchemaRegistryContainer {
  def apply(bootstrapServers: String) = new SchemaRegistryContainer(bootstrapServers)

  case class Def(
    bootstrapServers: String
  ) extends ContainerDef {

    override type Container = SchemaRegistryContainer

    override def createContainer(): SchemaRegistryContainer = {
      new SchemaRegistryContainer(bootstrapServers)
    }
  }
}

class QuickstartSuite extends CatsEffectSuite with ScalaCheckSuite with TestContainersForAll {
  override type Containers = KafkaContainer and SchemaRegistryContainer and LocalStackV2Container and PostgreSQLContainer

  override def startContainers(): Containers = {
    val network = Network.newNetwork()
    lazy val kafka = KafkaContainer().configure(_.setNetwork(network))
    lazy val registry = SchemaRegistryContainer("PLAINTEXT://" + kafka.networkAliases.headOption.getOrElse("localhost") + ":9092").configure { c =>
      val _ = c.dependsOn(kafka)
      c.setNetwork(network)
    }
    lazy val localstack = LocalStackV2Container(services = List(LS.Service.S3))
    lazy val postgres = PostgreSQLContainer()

    kafka.start()
    registry.start()
    localstack.start()
    postgres.start()

    kafka and registry and localstack and postgres
  }

  property("fails successfully") {
    withContainers { case kafka and registry and localstack and postgres =>
      val consumerSettings =
        ConsumerSettings[IO, String, String]
          .withAutoOffsetReset(AutoOffsetReset.Earliest)
          .withBootstrapServers(kafka.bootstrapServers)
          .withGroupId("group")

      val producerSettings =
        ProducerSettings[IO, String, String]
          .withBootstrapServers(kafka.bootstrapServers)

      // Inside your test body you can do with your containers whatever you want to
      falsified
    }
  }
}
