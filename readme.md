# Quickstart Example
An instance of the template app for use in adding features to an executable implementation.

## Running the app
There's no external dependencies or configs so you can start the app with
```shell
sbt run
```

## API:
There are two main apps, the _Greeting_ app and the _Peano_ app. These apps are bound to different ports to resemble public and private services.
#### Public App
The public "greeting" app is hosted an 8081 and can be accessed with a GET request to 
```
http://0.0.0.0:8081/hello/name
```

It replies by saying "hello ${name}" 

####Private App
The private Peano app converts an integer into its Peano representation, the service is hosted on port 8082.
It can be accessed via  
```
http://0.0.0.0:8082/peanoConvert/3
```
The result should be
```json
{
  "peano": "Successor(Successor(Successor(0)))"
}
```
The '3' can be replaced with a different positive integer of your choice.

_WARNING_ The service does not have robust error handling for negative or extremely large values
