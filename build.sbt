val Http4sVersion = "0.21.20"
val DoobieVersion = "0.13.4"
val CirceVersion = "0.13.0"
val TapirVersion = "0.17.20"
val SttpVersion = "3.3.9"
val DeclineVersion = "1.4.0"
val RefinedVersion = "0.9.27"
val NewtypeVersion = "0.4.4"
val SquantsVersion = "1.8.1"
val Fs2KafkaVersion = "1.7.0"
val Fs2AwsVersion = "3.1.1"
val Trace4CatsVersion = "0.11.0"
val EpimetheusVersion = "0.4.2"
val LogstageVersion = "1.0.8"
val MunitVersion = "0.7.26"
val LogbackVersion = "1.2.5"
val MunitCatsEffectVersion = "1.0.0"
val TestContainersVersion = "0.39.5"
val ScalaCheckShapelessVersion = "1.2.5"

resolvers ++= List(
  "Artifactory" at "https://kaluza.jfrog.io/artifactory/maven/",
  "confluent" at "https://packages.confluent.io/maven/"
)

lazy val root = (project in file("."))
  .configs(IntegrationTest)
  .settings(
    Defaults.itSettings,
    organization := "com.qomplx",
    name := "quickstart",
    version := "0.0.1-SNAPSHOT",
    scalaVersion := "2.13.3",
    IntegrationTest / fork := true,
    wartremoverErrors ++= Warts.unsafe.filterNot(_ == Wart.Any),  // fs2 code likes to infer intermediate `Any`s
    libraryDependencies ++= Seq(
      "org.http4s"                      %% "http4s-circe"                                % Http4sVersion,
      "org.http4s"                      %% "http4s-dsl"                                  % Http4sVersion,
      "io.circe"                        %% "circe-generic"                               % CirceVersion,
      "org.http4s" %% "http4s-blaze-server" % Http4sVersion,
      "org.http4s" %% "http4s-blaze-client" % Http4sVersion,
      "org.tpolecat"                    %% "doobie-postgres"                             % DoobieVersion,
      "org.tpolecat"                    %% "doobie-hikari"                               % DoobieVersion,
      "com.github.fd4s"                 %% "fs2-kafka"                                   % Fs2KafkaVersion,
      "com.github.fd4s"                 %% "fs2-kafka-vulcan"                            % Fs2KafkaVersion,
      "com.github.fd4s"                 %% "vulcan-generic"                              % "1.6.0",
      "com.ovoenergy"                   %% "ciris-kubernetes"                            % "1.2.4",
      "com.softwaremill.sttp.tapir"     %% "tapir-http4s-server"                         % TapirVersion,
      "com.softwaremill.sttp.tapir"     %% "tapir-json-circe"                            % TapirVersion,
      "com.softwaremill.sttp.tapir"     %% "tapir-openapi-docs"                          % TapirVersion,
      "com.softwaremill.sttp.tapir"     %% "tapir-openapi-circe-yaml"                    % TapirVersion,
      "com.softwaremill.sttp.tapir"     %% "tapir-sttp-client"                           % TapirVersion,
      "com.softwaremill.sttp.client3"   %% "http4s-ce2-backend"                          % SttpVersion,
      "com.softwaremill.sttp.client3"   %% "circe"                                       % SttpVersion,
      "com.monovore"                    %% "decline-effect"                              % DeclineVersion,
      "eu.timepit"                      %% "refined"                                     % RefinedVersion,
      "io.estatico"                     %% "newtype"                                     % NewtypeVersion,
      "org.typelevel"                   %% "squants"                                     % SquantsVersion,
      "io.laserdisc"                    %% "fs2-aws-s3"                                  % Fs2AwsVersion,
      "io.janstenpickle"                %% "trace4cats-sttp-client3"                     % Trace4CatsVersion,
      "io.janstenpickle"                %% "trace4cats-sttp-tapir"                       % Trace4CatsVersion,
      "io.janstenpickle"                %% "trace4cats-fs2"                              % Trace4CatsVersion,
      "io.janstenpickle"                %% "trace4cats-avro-kafka-exporter"              % Trace4CatsVersion,
      "io.janstenpickle"                %% "trace4cats-avro-kafka-consumer"              % Trace4CatsVersion,
      "io.janstenpickle"                %% "trace4cats-jaeger-thrift-exporter"           % Trace4CatsVersion,
      "io.chrisdavenport"               %% "epimetheus-http4s"                           % EpimetheusVersion,
      "io.7mind.izumi"                  %% "logstage-sink-slf4j"                         % LogstageVersion,
      "org.scalameta"                   %% "munit"                                       % MunitVersion               % "test,it",
      "org.scalameta"                   %% "munit-scalacheck"                            % MunitVersion               % "test,it",
      "org.typelevel"                   %% "munit-cats-effect-2"                         % MunitCatsEffectVersion     % "test,it",
      "com.github.alexarchambault"      %% "scalacheck-shapeless_1.14"                   % ScalaCheckShapelessVersion % "test,it",
      "com.dimafeng"                    %% "testcontainers-scala-munit"                  % TestContainersVersion      % "it",
      "com.dimafeng"                    %% "testcontainers-scala-postgresql"             % TestContainersVersion      % "it",
      "com.dimafeng"                    %% "testcontainers-scala-kafka"                  % TestContainersVersion      % "it",
      "com.dimafeng"                    %% "testcontainers-scala-localstack-v2"          % TestContainersVersion      % "it",      
      "software.amazon.awssdk"           % "s3"                                          % "2.15.7"                   % "it",
      "com.amazonaws"                    % "aws-java-sdk-s3"                             % "1.11.479"                 % "it",
      "ch.qos.logback"                   %  "logback-classic"                            % LogbackVersion             % "runtime",
      "org.scalameta"                   %% "svm-subs"                                    % "20.2.0"
    ),
    addCompilerPlugin("org.typelevel"  % "kind-projector" % "0.11.3" cross CrossVersion.full),
    addCompilerPlugin("com.olegpy"    %% "better-monadic-for" % "0.3.1"),
    testFrameworks += new TestFramework("munit.Framework")
  )

libraryDependencies += {
  val version = scalaBinaryVersion.value match {
    case "2.10" => "1.0.3"
    case _ ⇒ "2.3.8"
  }
  "com.lihaoyi" % "ammonite" % version % "test" cross CrossVersion.full
}

Test / sourceGenerators += Def.task {
  val file = (Test / sourceManaged).value / "amm.scala"
  IO.write(file, """object amm extends App { ammonite.Main.main(args) }""")
  Seq(file)
}.taskValue

// Optional, required for the `source` command to work
(Test / fullClasspath) ++= {
  (Test / updateClassifiers).value
    .configurations
    .find(_.configuration.name == Test.name)
    .get
    .modules
    .flatMap(_.artifacts)
    .collect{case (a, f) if a.classifier == Some("sources") => f}
}
